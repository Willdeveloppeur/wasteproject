DBNAME:= starter#CHANGE ME IF CHANGED IN DOCKER-COMPOSE
DBUSER:= starter#CHANGE ME IF CHANGED IN DOCKER-COMPOSE
DBCONTAINER := starter_database#CHANGE ME IF CHANGED IN DOCKER-COMPOSE
SERVER := starter_server#I'M CONTAINER NAME, CHANGE ME IF CHANGED IN DOCKER-COMPOSE
MYSQL_PASSWORD := changeMe#verify in docker-compose override sample
#NO SPACE AFTER VARIABLE VALUES 

UID := $(shell id -u)
D := docker
DC := docker-compose
DATETIME := $(shell date '+%Y-%m-%d-%H-%M-%S')
CURRENT_PATH := $(shell pwd)
CURRENT_APP := $(shell basename $(CURRENT_PATH) | sed "s/\_//g")
NETWORK := $(CURRENT_APP)_default

HELP_FUN = \
	%help; \
	while(<>) { \
		if(/^([a-z0-9_-]+):.*\#\#(?:@(\w+))?\s(.*)$$/) { \
			push(@{$$help{$$2 // 'options'}}, [$$1, $$3]); \
		} \
	}; \
	print "usage: make [target]\n\n"; \
	for ( sort keys %help ) { \
		print "$$_:\n"; \
		printf("\033[36m  %-20s %s\033[0m\n", $$_->[0], $$_->[1]) for @{$$help{$$_}}; \
		print "\n"; \
	}

.PHONY: help build rebuild start stop clean check_clean
.DEFAULT: all

help:
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)

# docker-compose.override.yml:
# 	@sed -e "s/{USER_ID}/$(UID)/g" \
# 		-e "s/{MYSQL_PASSWORD}/$(MYSQL_PASSWORD)/g" \
# 		docker-compose.override.sample.yml > docker-compose.override.yml

all: build start base ##@target Launch project with build then start
	
build: ##@target Build project (build)
	$(DC) pull
	$(DC) build

start:
	@tput setaf 7; echo "Launching Containers..."
	@$(DC) up -d
	@tput setaf 2; echo "Containers Launched"

	# PHP USE
	@tput setaf 7; echo "Creating miscellaneous directory in www folder..."
	@-mkdir www/miscellaneous
	@tput setaf 2; echo "miscellaneous directory created..."
	@tput setaf 7; echo "Creating sql.php file..."
	@cp makeRessources/sql.php www/miscellaneous/sql.php
	@tput setaf 2; echo "sql.php file created in miscellaneous directory..."
	@tput setaf 7; echo "Getting database container IP..."
	@cp makeRessources/sqlip.txt .
	@sed -i 's/777dbname/$(DBCONTAINER)/g' sqlip.txt
	@bash sqlip.txt
	@rm sqlip.txt
	@tput setaf 2; echo "Database container IP retrieved"
	@tput setaf 7; echo "Inputing database info into sql.php file..."
	@sed -i 's/777dbname/$(DBNAME)/g' www/miscellaneous/sql.php 
	@sed -i 's/777username/$(DBUSER)/g' www/miscellaneous/sql.php 
	@sed -i 's/777password/$(MYSQL_PASSWORD)/g' www/miscellaneous/sql.php 
	@tput setaf 2; echo "sql.php file now contains database info"

	# WORDPRESS USE

	# @tput setaf 7; echo "Launching Containers..."
	# @$(DC) up -d
	# @tput setaf 2; echo "Containers Launched"
	# @tput setaf 7; echo "Making wp-config file..."
	# @sudo cp makeRessources/wp-config.php www/wp-config.php
	# @sed -i 's/777dbname/$(DBNAME)/g' www/wp-config.php
	# @sed -i 's/777username/$(DBUSER)/g' www/wp-config.php
	# @sed -i 's/777password/$(MYSQL_PASSWORD)/g' www/wp-config.php
	# @sed -i 's/777host/$(DBCONTAINER)/g' www/wp-config.php
	# @tput setaf 2; echo "wp-config file made"
	# @tput setaf 7; echo "Launching PhpMyAdmin..."
	# @$(D) run --name $(SERVER)_phpmyadmin -d -e PMA_USER=$(DBUSER) -e PMA_PASSWORD=$(MYSQL_PASSWORD) --network $(NETWORK) --link $(DBCONTAINER):db -p 8080:80 phpmyadmin/phpmyadmin
	# @tput setaf 2; echo "PhpMyAdmin launched"
	# @tput setaf 7; echo "Checking if Database is runnning yet..."
	# @docker inspect -f '{{.State.Running}}' $(DBCONTAINER)
	# @tput setaf 2; echo "Database is running !"

restart: stop start ##@target Restart all containers (stop start)

cleandok:
	$(D) stop $$(docker ps -a -q)
	$(D) rm $$(docker ps -a -q)
	make portainer

stop: dbdump ##@target Clean all volume content (down -v) an delete docker-compose.override.yml
	@tput setaf 7; echo "Stopping containers..."
	@$(DC) stop
	@tput setaf 2; echo "All containers stopped"

	# PHP USE
	@tput setaf 7; echo "Removing sql.php file..."
	-rm www/miscellaneous/sql.php
	@tput setaf 2; echo "sql.php file removed"

	# # WORDPRESS USE
	# @tput setaf 7; echo "Removing wp-config file..."
	# @-sudo rm -r www/wp-config.php
	# @tput setaf 2; echo "wp-config file removed"
	
	@tput setaf 7; echo "Removing PhpMyadmin Container..."
	@-$(D) rm -f $(SERVER)_phpmyadmin
	@tput setaf 2; echo "PhpMyAdmin Container removed"
	

kill: dbdump stop
	@tput setaf 7; echo "Stopping and removing containers..."
	@-$(DC) down -v
	@tput setaf 2; echo "Containers stopped and removed"
	@tput setaf 7; echo "Removing docker-compose.override.yml file..."
	@-rm docker-compose.override.yml
	@tput setaf 2; echo "docker-compose.override.yml file removed"

rebuild: kill start

cleanall: kill cleanroot ##@target removes all containers and deletes everything

base: ##@target put base files in www/
	cp -r makeRessources/base/* www/
	sed -i 's/777dbname/$(DBNAME)/g' www/index.php
	sed -i 's/777username/$(DBUSER)/g' www/index.php
	sed -i 's/777password/$(MYSQL_PASSWORD)/g' www/index.php

logs: 
	$(DC) logs -f
	
shell: ##@target Open a shell for the server configuration
	gnome-terminal -- $(D) exec -it $(SERVER) bash

git: 
	@tput setaf 7; git add .
	git commit -a
	git push

teamgit: dbdump latestdump stop git

## Database targets

mysql: ##@target Open a shell for mysql
	gnome-terminal -- $(D) exec -it $(DBCONTAINER) sh -c 'mysql -u "$$MYSQL_USER" -p"$$MYSQL_PASSWORD" --database=$$MYSQL_DATABASE'

dbdump: ##@database Dump database from container into 'sql/'
	@tput setaf 7; echo "Creating Dump file..."
	@-$(D) exec -i $(DBCONTAINER) sh -c 'mysqldump -u "$$MYSQL_USER" -p"$$MYSQL_PASSWORD" --databases "$$MYSQL_DATABASE"' > sql/$(DATETIME)dump.sql
	@tput setaf 2; echo "Dump file created"
	@-tput setaf 7; git checkout develop
	@tput setaf 7; echo "Adding SQL files to git..."
	@-tput setaf 7; git add sql/*
	@tput setaf 2; echo "SQL files added to git"
	@tput setaf 7; echo "Committing SQL files to git..."
	@-tput setaf 7; git commit -m "new database dump"
	@tput setaf 2; echo "SQL files committed"

dbimport: ##@database Import database from 'sql/dump.sql'
	@tput setaf 2; echo -n "Import dump.sql ? [Y/N] " && read ans && [ $${ans:-N} = y ]
	@tput setaf 7; echo "Importing SQL file into Database..."
	@$(D) exec -i $(DBCONTAINER) sh -c 'mysql -u "$$MYSQL_USER" -p"$$MYSQL_PASSWORD" "$$MYSQLDATABASE"' < sql/dump.sql
	@tput setaf 2; echo "SQL file imported to database !"

cleanroot: ##@target remove all files in the www folder
	@tput setaf 2; echo -n "Remove all files in www folder ? [Y/N] " && read ans && [ $${ans:-N} = y ]
	@tput setaf 7; echo "Removing all files located in www folder..."
	-sudo rm -r www/*
	@tput setaf 2; echo "All files in www folder removed"

cleandumps: 
	@tput setaf 2; echo -n "Removed all unused database dumps ? [Y/N] " && read ans && [ $${ans:-N} = y ]
	@tput setaf 7; echo "Creating tmp directory..."
	@-mkdir tmp
	@tput setaf 2; echo "tmp folder created"
	@tput setaf 7; echo "Moving reset.sql and dump.sql files to tmp folder..."
	@mv sql/reset.sql tmp/reset.sql
	@-mv sql/dump.sql tmp/dump.sql
	@tput setaf 2; echo "reset.sql and dump.sql file moved"
	@tput setaf 7; echo "Removing all files in sql folder..."
	@-rm sql/*
	@tput setaf 2; echo "All files in sql folder removed"
	@tput setaf 7; echo "Moving back reset.sql and dump.sql files..."
	@mv tmp/reset.sql sql/reset.sql
	@-mv tmp/dump.sql sql/dump.sql
	@tput setaf 2; echo "reset.sql and dump.sql files moved back to sql folder"
	@tput setaf 7; echo "Removing tmp folder..."
	@rm -r tmp
	@tput setaf 2; echo "tmp folder removed"

latestdump: ##@target rename latest dump to dump.sql
	@tput setaf 2; echo -n "Make latest database dump the main dump.sql ? [Y/N] " && read ans && [ $${ans:-N} = y ]
	@tput setaf 7; echo "removing current dump.sql file..."
	@-rm sql/dump.sql
	@tput setaf 2; echo "Current dump.sql file deleted"
	@tput setaf 7; echo "Creating tmp directory..."
	@-mkdir tmp
	@tput setaf 2; echo "tmp directory created"
	@tput setaf 7; echo "Moving reset.sql file to tmp folder..."
	@mv sql/reset.sql tmp/reset.sql
	@tput setaf 2; echo "reset.sql file was moved to tmp folder"
	@tput setaf 7; echo "Finding latest dump file and renaming it dump.sql..."
	@ls -tr sql/| tail -n 1 | xargs -I{} mv sql/{} sql/dump.sql
	@tput setaf 2; echo "File found and renamed"
	@tput setaf 7; echo "Moving back reset.sql file..."
	@mv tmp/reset.sql sql/reset.sql
	@tput setaf 2; echo "reset.sql file was moved back"
	@tput setaf 7; echo "Removing tmp folder..."
	@-rm -r tmp
	@tput setaf 2; echo "tmp folder removed"

cleandb: dbdump ##@target clean all databases
	$(D) exec -i $(DBCONTAINER) sh -c 'mysql -u "$$MYSQL_USER" -p"$$MYSQL_PASSWORD" "$$MYSQLDATABASE"' < sql/reset.sql

wordpress: all cleanroot ##@target download and install wordpress
	wget https://wordpress.org/latest.zip -P www
	unzip www/latest.zip -d www
	rm www/latest.zip
	mv www/wordpress/* www/
	rm -r www/wordpress
	rm www/wp-config-sample.php
	cp makeRessources/wp-config.php www/wp-config.php
	sed -i 's/777dbname/$(DBNAME)/g' www/wp-config.php
	sed -i 's/777username/$(DBUSER)/g' www/wp-config.php
	sed -i 's/777password/$(MYSQL_PASSWORD)/g' www/wp-config.php
	sed -i 's/777host/$(DBCONTAINER)/g' www/wp-config.php
	$(D) exec -it $(SERVER) sh -c 'mv  wp-content/themes/twentytwenty wp-content/twentytwenty'
	$(D) exec -it $(SERVER) sh -c 'rm -r wp-content/themes/*'
	$(D) exec -it $(SERVER) sh -c 'mv  wp-content/twentytwenty wp-content/themes/twentytwenty'
	$(D) exec -it $(SERVER) sh -c 'rm -r wp-content/plugins/*'
	$(D) exec -it $(SERVER) sh -c 'chown www-data:www-data  -R *'
	$(D) exec -it $(SERVER) sh -c 'find . -type d -exec chmod 755 {} \;'
	$(D) exec -it $(SERVER) sh -c 'find . -type f -exec chmod 644 {} \;'
	$(D) exec -it $(SERVER) sh -c 'curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar'
	$(D) exec -it $(SERVER) sh -c 'chmod +x wp-cli.phar'
	$(D) exec -it $(SERVER) sh -c 'mv wp-cli.phar /usr/local/bin/wp'

portainer: ##@target launch portainer
	$(D) volume create portainer_data
	$(D) run -d -p 9000:9000 -p 8000:8000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

phpinfo: ##@target display php info
	cp makeRessources/phpinfo.php www/php.php

sass:
	gnome-terminal -- bash -c "sass --watch www/style/sass:www/style/css/"

phpmyadmin: 
	$(D) run --name $(SERVER)_phpmyadmin -d -e PMA_USER=$(DBUSER) -e PMA_PASSWORD=$(MYSQL_PASSWORD) --network $(NETWORK) --link $(DBCONTAINER):db -p 8080:80 phpmyadmin/phpmyadmin

phpshell: ##@target Open a shell for the server configuration
	gnome-terminal -- $(D) exec -it $(SERVER)_phpmyadmin bash

wp-cli: ##@target install wp-cli
	-$(D) exec -it $(SERVER) sh -c 'curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar'
	-$(D) exec -it $(SERVER) sh -c 'chmod +x wp-cli.phar'
	-$(D) exec -it $(SERVER) sh -c 'mv wp-cli.phar /usr/local/bin/wp'
