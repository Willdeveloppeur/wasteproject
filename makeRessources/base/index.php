<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/css/index.css">
    <title>Starter</title>
</head>
<body>
    <h1>Welcome !</h1>
    <?php require_once('miscellaneous/sql.php');
        if ($conn) {
            ?><h2 class='green'>Database connection : success!</h2><?php
        } else {
            ?><h2 class='red'>Database connection : failed!</h2>
            <h3 class="error red">Check errors above !</h3>
            <h3 class="error red">Possible causes :</h3>
            <ol>
                <li class="error red">Database container is not started yet, wait a minute and try again.</li>
                <li class="error red">Data in makefile, docker-compose.yml and docker-compose-override.yml do not match, check them and try again.</li>
                <li class="error red">Data has been modifed in 'miscellaneous/sql.php"</li>
            </ol><?php
        }
    ?>
    <br>
    <h4 class="db-info"> | SQL info | </h4>
    <h4 class="db-info">User : 777username </h4>
    <h4 class="db-info">Password : 777password </h4>
    <h4 class="db-info">Database : 777dbname </h4>
    <br>
    <h4 class="db-info blue">To convert to Wordpress, type "make wordpress" in your terminal, and follow makefile instructions around "start".</h4>
<style>

* {
    font-family: sans-serif;
}

h1 {
    text-align: center;
    font-size: 45pt;
}

h2 {
    text-align: center;
    font-size: 22pt;
}

.error, .db-info {
    font-size: 20pt;
}

.db-info {
    text-align: center;
}

.red {
    color: red;
}

.green {
    color: green;
}

.blue {
    color: blue;
}

</style>
    <script src="js/index.js"></script>
</body>
</html>
