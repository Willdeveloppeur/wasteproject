<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/css/index.css">
    <title>Control Waste</title>
</head>

<body>
    <h1>Welcome !</h1>
    <p>Projet Waste management : 1ere partie / schéma UML</p>
    
    <style>
        * {
            font-family: sans-serif;
        }

        p {
            margin: 10vw;
            font-size: 25px;
        }

        h1 {
            text-align: center;
            font-size: 45pt;
        }

        h2 {
            text-align: center;
            font-size: 22pt;
        }

        .error,
        .db-info {
            font-size: 20pt;
        }

        .db-info {
            text-align: center;
        }

        .red {
            color: red;
        }

        .green {
            color: green;
        }

        .blue {
            color: blue;
        }
    </style>
    <script src="js/index.js"></script>
</body>

</html>